﻿namespace System_Information
{
	partial class Form
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Info_panel = new System.Windows.Forms.Panel();
			this.SystemInfo_dataGridView = new System.Windows.Forms.DataGridView();
			this.panel1 = new System.Windows.Forms.Panel();
			this.KeyTest_button = new System.Windows.Forms.Button();
			this.Search_button = new System.Windows.Forms.Button();
			this.Win32API_comboBox = new System.Windows.Forms.ComboBox();
			this.Info_panel.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.SystemInfo_dataGridView)).BeginInit();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// Info_panel
			// 
			this.Info_panel.Controls.Add(this.SystemInfo_dataGridView);
			this.Info_panel.Location = new System.Drawing.Point(12, 58);
			this.Info_panel.Name = "Info_panel";
			this.Info_panel.Size = new System.Drawing.Size(776, 380);
			this.Info_panel.TabIndex = 0;
			// 
			// SystemInfo_dataGridView
			// 
			this.SystemInfo_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.SystemInfo_dataGridView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SystemInfo_dataGridView.Location = new System.Drawing.Point(0, 0);
			this.SystemInfo_dataGridView.Name = "SystemInfo_dataGridView";
			this.SystemInfo_dataGridView.ReadOnly = true;
			this.SystemInfo_dataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
			this.SystemInfo_dataGridView.Size = new System.Drawing.Size(776, 380);
			this.SystemInfo_dataGridView.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.KeyTest_button);
			this.panel1.Controls.Add(this.Search_button);
			this.panel1.Controls.Add(this.Win32API_comboBox);
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(776, 40);
			this.panel1.TabIndex = 0;
			// 
			// KeyTest_button
			// 
			this.KeyTest_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.KeyTest_button.Location = new System.Drawing.Point(508, 6);
			this.KeyTest_button.Name = "KeyTest_button";
			this.KeyTest_button.Size = new System.Drawing.Size(264, 28);
			this.KeyTest_button.TabIndex = 2;
			this.KeyTest_button.Text = "Test Hardware Key";
			this.KeyTest_button.UseVisualStyleBackColor = true;
			this.KeyTest_button.Click += new System.EventHandler(this.KeyTest_button_Click);
			// 
			// Search_button
			// 
			this.Search_button.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Search_button.Location = new System.Drawing.Point(397, 6);
			this.Search_button.Name = "Search_button";
			this.Search_button.Size = new System.Drawing.Size(98, 28);
			this.Search_button.TabIndex = 1;
			this.Search_button.Text = "Search";
			this.Search_button.UseVisualStyleBackColor = true;
			this.Search_button.Click += new System.EventHandler(this.Search_button_Click);
			// 
			// Win32API_comboBox
			// 
			this.Win32API_comboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.Win32API_comboBox.FormattingEnabled = true;
			this.Win32API_comboBox.Items.AddRange(new object[] {
            "Win32_ComputerSystem",
            "Win32_DiskDrive",
            "Win32_DiskDrive_USB",
            "Win32_OperatingSystem",
            "Win32_Processor",
            "Win32_ProgramGroup",
            "Win32_SystemDevices",
            "Win32_StartupCommand"});
			this.Win32API_comboBox.Location = new System.Drawing.Point(3, 6);
			this.Win32API_comboBox.Name = "Win32API_comboBox";
			this.Win32API_comboBox.Size = new System.Drawing.Size(381, 28);
			this.Win32API_comboBox.TabIndex = 1;
			// 
			// Form
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(800, 450);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.Info_panel);
			this.MaximizeBox = false;
			this.Name = "Form";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "System Info";
			this.Info_panel.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.SystemInfo_dataGridView)).EndInit();
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Panel Info_panel;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Button KeyTest_button;
		private System.Windows.Forms.Button Search_button;
		private System.Windows.Forms.ComboBox Win32API_comboBox;
		private System.Windows.Forms.DataGridView SystemInfo_dataGridView;
	}
}

