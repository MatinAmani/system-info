﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Management;
using System.Windows.Forms;
using System.Data;

namespace System_Information
{
	public partial class Form : System.Windows.Forms.Form
	{
		public Form()
		{
			InitializeComponent();
		}

		private void Search_button_Click(object sender, EventArgs e)
		{
			switch (Win32API_comboBox.Text)
			{
				case "Win32_DiskDrive_USB":
					SystemInfo_dataGridView.DataSource = Win32DiskDrive_USB_Info();
					break;

				case "":
					MessageBox.Show("Please Select an API From ComboBox.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
					break;

				default:
					SystemInfo_dataGridView.DataSource = Win32_Info(Win32API_comboBox.SelectedItem.ToString());
					break;
			}
		}

		private ArrayList Win32_Info(string query)
		{
			ManagementObjectSearcher objectSearcher;
			ArrayList arrayList = new ArrayList();
			try
			{
				objectSearcher = new ManagementObjectSearcher("Select * from " + query);
				foreach (ManagementObject managementObject in objectSearcher.Get())
				{
					PropertyDataCollection dataCollection = managementObject.Properties;
					foreach (PropertyData propertyData in dataCollection)
					{
						arrayList.Add(propertyData);
					}
				}
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.ToString(), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
			return arrayList;
		}

		private ArrayList Win32DiskDrive_USB_Info()
		{
			return Win32_Info("Win32_DiskDrive where InterfaceType='USB'");
		}

		private void KeyTest_button_Click(object sender, EventArgs e)
		{
			string serialNumber = "";
			SystemInfo_dataGridView.DataSource = Win32DiskDrive_USB_Info();
			foreach (DataGridViewRow row in SystemInfo_dataGridView.Rows)
			{
				DataGridViewRow dataRow = row;
				if (dataRow.Cells[0].Value.ToString() == "SerialNumber")
				{
					serialNumber = dataRow.Cells[1].Value.ToString();
					break;
				}
			}
			SystemInfo_dataGridView.DataSource = null;
			if (serialNumber == "047C06A290A0")
			{
				MessageBox.Show("Correct Key Inserted.", "Unlocked", MessageBoxButtons.OK, MessageBoxIcon.Information);
			}
			else
			{
				MessageBox.Show("Key NOT Correct.", "Locked", MessageBoxButtons.OK, MessageBoxIcon.Warning);
			}
		}
	}
}
